import os
import argparse
import sys

import numpy as np
import tensorflow as tf

import json
import glob
import time
import matplotlib.pyplot as plt
from itertools import cycle
#from matplotlib.pyplot import cm
import matplotlib.cm as cm

import scipy.misc as misc
import sklearn.metrics as metric
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

#
import model
import input

##
IMAGE_SIZE = input.IMAGE_SIZE
BATCH_SIZE = input.BATCH_SIZE


def ____show_figures(images, labels, names, probs,
                  image_width = IMAGE_SIZE,
                  image_height = IMAGE_SIZE,
                  num_rows = 5,
                  num_cols = 10,
                  figsize = (16,9)):
    plt.figure(figsize=figsize)
    num_figs = num_rows * num_cols
    num_chars = 5   # num of chars to show in names

    for j in range(num_figs):
        plt.subplot(num_rows, num_cols, j + 1)
        plt.imshow(images[j].reshape([image_height, image_width]), cmap='gray')
        plt.axis('off')
        name = 'dummy' # names[j][:num_chars]
        plt.title( name + '(N:%.3f)' % (probs[j][0]) \
                      if labels[j] == 0 \
                      else name + '(P:%.3f)' % (probs[j][1]),
                  fontsize=9)

    # mng = plt.get_current_fig_manager()
    # mng.window.showMaximized()
    plt.show()

def _save_figures(
        images,
        title,
        num_rows = 4,
        num_cols = 5,
        figsize = (9,7),
        show_fig = False,
        save_path = None,
        max_samples = 3
):
    num_figs = num_rows * num_cols
    num_imgs = images.shape[0]

    plt.figure(figsize=figsize)
    j = 0
    s = 0
    for i in range(num_imgs):
        if s == max_samples: break
        plt.subplot(num_rows, num_cols, j + 1)
        plt.imshow(images[i].reshape([IMAGE_SIZE, IMAGE_SIZE]), cmap='gray')
        plt.axis('off')
        plt.title('', fontsize=9)
        j += 1
        if j == num_figs:
            if show_fig: plt.show()
            if not save_path is None:
                plt.savefig(os.path.join(save_path, title + '-%03d.png'%(s)))
                plt.clf()
            j = 0
            s += 1

    if s < max_samples:
        if show_fig: plt.show()
        if not save_path is None:
            plt.savefig(os.path.join(save_path, title + '-%03d.png'%(s)))



def get_prec_sens_spec(probs, lbls, thres=0.5):
    pred = probs[:, 1] > thres
    prec = metric.accuracy_score(y_true=lbls,
                                  y_pred=pred)
    cmat = metric.confusion_matrix(y_true=lbls,
                                   y_pred=pred)
    tn, fp, fn, tp = cmat.ravel()

    sens = float(tp) / (tp + fn)
    spec = float(tn) / (tn + fp)

    return prec, sens, spec

def _get_num_examples(data_path):
    num_examples = 0
    rec_filenames = glob.glob(os.path.join(data_path, '*.tfrecord'))
    for fn in rec_filenames:
        for _ in tf.python_io.tf_record_iterator(fn):
            num_examples += 1
    return num_examples

def __deprecated_draw_ROC(lbls, probs,
              title,
              figsize=(5,5),
              show_fig=False,
              save_path=None):

    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    num_classes = probs.shape[1]
    onehot = np.zeros((lbls.shape[0], num_classes))
    onehot[np.arange(lbls.shape[0]), lbls.astype('int32')] = 1

    plt.figure(figsize=figsize)
    lw = 1
    colors = cm.rainbow(np.linspace(0, 1, num_classes))
    if num_classes == 2: num_classes = 1
    for i, color in zip(range(num_classes), colors):
        fpr[i], tpr[i], _ = metric.roc_curve(onehot[:, i], probs[:, i])
        roc_auc[i] = metric.auc(fpr[i], tpr[i])
        # print fpr[i], tpr[i], roc_auc[i]

        plt.plot(fpr[i], tpr[i], color=color,
                 lw=lw, label='ROC of class %d (area = %0.2f)' % (i,roc_auc[i]))

    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="lower right")

    if not save_path is None:
        plt.savefig(os.path.join(save_path, title+'.png'))
    if show_fig:
        plt.show()

def __deprecated_get_result(
        lbls,
        probs,
        res_name,
        save_path='None'
):
    num_examples = probs.shape[0]
    num_classes = probs.shape[1]

    # one-hot encoding
    onehot = np.zeros((lbls.shape[0], num_classes))
    onehot[np.arange(lbls.shape[0]), lbls.astype('int32')] = 1

    pred = np.argmax(probs, axis=1)
    accv = metric.accuracy_score(y_true=lbls, y_pred=pred)
    cmat = metric.confusion_matrix(y_true=lbls, y_pred=pred)

    sens = dict()
    spec = dict()
    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    for i in range(0, num_classes):
        fpr[i], tpr[i], _ = metric.roc_curve(onehot[:, i], probs[:, i])
        roc_auc[i] = metric.auc(fpr[i], tpr[i])

        tn = np.sum(cmat[:i, :i])
        fp = np.sum(cmat[:i, i:])
        fn = np.sum(cmat[i:, :i])
        tp = np.sum(cmat[i:, i:])
        sens[i] = float(tp) / max(tp + fn, 0.0000001)
        spec[i] = float(tn) / max(tn + fp, 0.0000001)

    if not save_path is None:
        log = {
            '1 res_name': res_name,
            '2 num_examples': num_examples,
            '3 num_classes': num_classes,
            '4 roc_auc': _comma_join(roc_auc.values()),
            '5 confusion_matrix': _comma_join(cmat.ravel()),
            '6 sensitivity': _comma_join(sens.values()),
            '7 specificity': _comma_join(spec.values()),
            '8 accuracy': accv
        }
        with open(os.path.join(save_path, res_name+'.txt'), 'w') as f:
            f.write(json.dumps(log, indent=4, sort_keys=True))
            f.close()

    return roc_auc, fpr, tpr, cmat, sens, spec, accv

def _draw_ROC(
        roc_auc, fpr, tpr, num_classes, title,
        figsize=(5,5),
        show_fig=False,
        save_path=None
):
    plt.figure(figsize=figsize)
    lw = 1
    colors = iter(cm.rainbow(np.linspace(0, 1, num_classes)))

    for i in range(1, num_classes):
        color = next(colors)
        plt.plot(fpr[i], tpr[i], color=color,
                 lw=lw, label='ROC [0-%d] (area = %0.2f)' % (i-1,roc_auc[i]))

    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(title)
    plt.legend(loc="lower right")

    if not save_path is None:
        plt.savefig(os.path.join(save_path, title+'.png'))
    if show_fig:
        plt.show()

def _comma_join(inlist):
    return ', '.join([str(i) for i in inlist])

def _get_binarized(lbls, probs, cut_idx = 1):
    assert cut_idx > 0 and cut_idx < probs.shape[1]

    _lbls = (lbls >= cut_idx).astype('int32')
    _probs = np.concatenate(
        (np.expand_dims(np.sum(probs[:, :cut_idx], axis=1), axis=-1),
         np.expand_dims(np.sum(probs[:, cut_idx:], axis=1), axis=-1)
        ), axis=1
    )

    return _lbls, _probs

def _tmp_test():
    #np.random.seed(1)

    lbls = np.random.randint(0, 4, size=(100,1))
    probs = np.random.uniform(size=(100,4))

    #print lbls
    #print probs

    lbls, probs = _get_binarized(lbls, probs, 2)

    #print lbls
    #print probs
    pred = np.argmax(probs, axis=1)
    accv = metric.accuracy_score(y_true=lbls, y_pred=pred)
    cmat = metric.confusion_matrix(y_true=lbls, y_pred=pred)
    tn = cmat[0, 0]
    fp = cmat[0, 1]
    fn = cmat[1, 0]
    tp = cmat[1, 1]
    accv2 = float(tp+tn) / (tp+fn+tn+fp)

    print accv, accv2
    print cmat

def ___optimal_cutoff(target, predicted):
    """ Find the optimal probability cutoff point for a classification model related to event rate
    Parameters
    ----------
    target : Matrix with dependent or target data, where rows are observations

    predicted : Matrix with predicted data, where rows are observations

    Returns
    -------
    list type, with optimal cutoff value

    """
    fpr, tpr, threshold = metric.roc_curve(target, predicted)
    i = np.arange(len(tpr))
    roc = pd.DataFrame({'tf' : pd.Series(tpr-(1-fpr), index=i), 'threshold' : pd.Series(threshold, index=i)})
    roc_t = roc.ix[(roc.tf-0).abs().argsort()[:1]]

    return list(roc_t['threshold'])

def _eval_binarized(lbls, probs):
    # Ensure inputs are binarized
    assert probs.shape[1] == 2

    # one-hot encoding
    onehot = np.zeros_like(probs)
    onehot[np.arange(lbls.shape[0]), lbls.astype('int32')] = 1

    fpr, tpr, thres = metric.roc_curve(onehot[:, 0], probs[:, 0])
    roc_auc = metric.auc(fpr, tpr)

    i = np.arange(len(tpr))
    roc = pd.DataFrame(
        {'tf': pd.Series(tpr - (1 - fpr), index=i),
         'threshold': pd.Series(thres, index=i)}
    )
    roc_t = roc.ix[(roc.tf - 0).abs().argsort()[:1]]
    opt_cutoff = list(roc_t['threshold'])
    #print opt_cutoff

    #pred = np.argmax(probs, axis=1)
    pred = np.zeros([probs.shape[0],1], dtype=np.int32)
    pred[probs[:,0] < opt_cutoff[0]] = 1
    cmat = metric.confusion_matrix(y_true=lbls, y_pred=pred)

    '''
    accv = metric.accuracy_score(y_true=lbls, y_pred=pred)
    tn = cmat[0, 0]
    fp = cmat[0, 1]
    fn = cmat[1, 0]
    tp = cmat[1, 1]
    sens = float(tp) / max(tp + fn, 0.0000001)
    spec = float(tn) / max(tn + fp, 0.0000001)
    accv2 = float(tp + tn) / (tp + fn + tn + fp)
    '''

    return roc_auc, fpr, tpr, cmat

def _get_result(
        lbls,
        probs,
        res_name,
        save_path='None'
):
    num_examples = probs.shape[0]
    num_classes = probs.shape[1]

    roc_auc = dict()
    fpr = dict()
    tpr = dict()
    cmat = dict()
    accv = dict()
    spec = dict()
    sens = dict()

    for i in range(1, num_classes):
        _lbls, _probs = _get_binarized(lbls, probs, cut_idx=i)
        roc_auc[i], fpr[i], tpr[i], cmat[i] = _eval_binarized(_lbls, _probs)

        tn = cmat[i][0, 0]
        fp = cmat[i][0, 1]
        fn = cmat[i][1, 0]
        tp = cmat[i][1, 1]
        sens[i] = float(tp) / max(tp + fn, 0.0000001)
        spec[i] = float(tn) / max(tn + fp, 0.0000001)
        accv[i] = float(tp + tn) / (tp + fn + tn + fp)

    if not save_path is None:
        log = {
            '1 res_name': res_name,
            '2 num_examples': num_examples,
            '3 num_classes': num_classes,
            '4 roc_auc': _comma_join(roc_auc.values()),
            '5 accuracy': _comma_join(accv.values()),
            '6 sensitivity': _comma_join(sens.values()),
            '7 specificity': _comma_join(spec.values()),
        }
        with open(os.path.join(save_path, res_name+'.txt'), 'w') as f:
            f.write(json.dumps(log, indent=4, sort_keys=True))
            f.close()

    return roc_auc, fpr, tpr, cmat, sens, spec, accv


def run_validation(
        data_path,
        exp_name,
        model_name,
        val_name,
        trial_serial,
        interval=10,
        max_keep=100
):

    val_data_path = os.path.join(data_path, exp_name, val_name)
    model_path = os.path.join(data_path, exp_name, model_name)
    # temporary checkpoints from training
    train_ckpt_path = os.path.join(model_path, 'train_ckpt')
    # selected checkpoints by validation
    trial_serial_str = '%03d'%(trial_serial)
    val_ckpt_path = os.path.join(model_path, '%s_ckpt-%s'%(val_name, trial_serial_str))
    input.recreate_directory(val_ckpt_path)

    batch_size = BATCH_SIZE
    num_classes = len(input.get_info(val_data_path, 'LABEL_LIST'))
    num_examples = _get_num_examples(val_data_path)

    print val_ckpt_path, num_examples

    with tf.Graph().as_default() as g:

        images, labels = input.input(
            data_path=val_data_path,
            batch_size=batch_size,
            num_epochs= 1,
            random_distortion=False,
            shuffle_batch=False
        )

        inference_name = 'inference_' + model_name
        if not inference_name in dir(model):
            raise ValueError('The model [%s] is not in model.py' %(model_name))

        logits = getattr(model, inference_name)(images=images, num_classes=num_classes, is_training=False)
        #logits = model.inference2(images=images, num_classes=num_classes, is_training=False)

        softmax = tf.nn.softmax(logits=logits, dim=-1)

        saver = tf.train.Saver()
        saver2 = tf.train.Saver(max_to_keep=max_keep)

        #summary_op = tf.summary.merge_all()
        #summary_writer = tf.summary.FileWriter(eval_dir, g)

        eval_step = 0
        curr_ckpt_path = ''
        stay_cnt = 0
        while True:
            #eval_once(saver, logits, softmax, num_examples, batch_size)

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(train_ckpt_path)
                if ckpt and ckpt.model_checkpoint_path:
                    if ckpt.model_checkpoint_path == curr_ckpt_path:
                        if stay_cnt > 10:
                            print ('No new checkpoint to restore')
                            return
                        stay_cnt += 1
                        time.sleep(interval)
                        continue
                    else:
                        curr_ckpt_path = ckpt.model_checkpoint_path
                        stay_cnt = 0
                        print ('Restoring: ' + curr_ckpt_path)
                        saver.restore(sess, curr_ckpt_path)
                        global_step = curr_ckpt_path.split('/')[-1].split('-')[-1]
                else:
                    print ('No checkpoint file found', model_path)
                    return

                try:
                    num_iter = int(np.ceil(float(num_examples) / batch_size))
                    step = 0

                    probs = np.empty([num_examples, num_classes])
                    imgs = np.empty([num_examples, IMAGE_SIZE, IMAGE_SIZE, 1])
                    lbls = np.zeros([num_examples], dtype=np.int32)
                    # nams = np.empty([num_iter*batch_size], dtype=object)

                    while step < num_iter:  # and not coord.should_stop():
                        prob, img, lbl = \
                            sess.run([softmax, images, labels])

                        probs[step * batch_size:step * batch_size + prob.shape[0]] = prob
                        imgs[step * batch_size:step * batch_size + img.shape[0]] = img
                        lbls[step * batch_size:step * batch_size + lbl.shape[0]] = lbl

                        step += 1

                    roc_auc, fpr, tpr, cmat, sens, spec, accv = \
                        _get_result(lbls, probs,
                                    res_name=".".join(
                                        [exp_name, model_name, global_step, val_name, trial_serial_str]
                                    ),
                                    save_path=None #os.path.join(data_path, 'result')
                                    )

                    print roc_auc

                    if roc_auc[1] > 0.94 and roc_auc[2] > 0.94:
                        saver2.save(sess,
                           save_path=os.path.join(val_ckpt_path, 'model.ckpt'),
                           global_step=int(global_step)
                           )


                except Exception as e:  # pylint: disable=broad-except
                    print (e)

            eval_step += 1
            print "Eval %d finished" % (eval_step)

            time.sleep(interval)


def ___run_test(
        data_path,
        exp_name,
        model_name,
        data_name='test'
):
    test_data_path = os.path.join(data_path, exp_name, data_name)
    model_path = os.path.join(data_path, exp_name, model_name, 'selected/')
    if not os.path.exists(model_path):
        model_path = os.path.join(data_path, exp_name, model_name)

    # eval_dir = DATA_PATH + 'log/'
    batch_size = BATCH_SIZE
    num_classes = len(input.get_info(test_data_path, 'LABEL_LIST'))
    num_examples = _get_num_examples(test_data_path)

    print model_path, num_classes, num_examples
    print test_data_path

    with tf.Graph().as_default() as g:

        images, labels = input.input(
            data_path=test_data_path,
            batch_size=batch_size,
            num_epochs=1,
            random_distortion=False,
            shuffle_batch=False
        )

        inference_name = 'inference_' + model_name
        if not inference_name in dir(model):
            raise ValueError('The model [%s] is not in model.py' % (model_name))

        logits = getattr(model, inference_name)(images=images, num_classes=num_classes, is_training=False)

        softmax = tf.nn.softmax(logits=logits, dim=-1)

        saver = tf.train.Saver()

        ckpt = tf.train.get_checkpoint_state(model_path)
        all_ckpt_paths = ckpt.all_model_checkpoint_paths
        for ckpt_path in all_ckpt_paths[-2:]:
            print ('Restoring: ' + ckpt_path)
            global_step = ckpt_path.split('/')[-1].split('-')[-1]

            with tf.Session() as sess:
                saver.restore(sess, ckpt_path)

                try:
                    num_iter = int(np.ceil(float(num_examples) / batch_size))
                    step = 0

                    probs = np.empty([num_examples, num_classes])
                    imgs = np.empty([num_examples, IMAGE_SIZE, IMAGE_SIZE, 1])
                    lbls = np.zeros([num_examples], dtype=np.int32)
                    # nams = np.empty([num_iter*batch_size], dtype=object)

                    while step < num_iter:  # and not coord.should_stop():
                        prob, img, lbl = \
                            sess.run([softmax, images, labels])

                        probs[step * batch_size:step * batch_size + prob.shape[0]] = prob
                        imgs[step * batch_size:step * batch_size + img.shape[0]] = img
                        lbls[step * batch_size:step * batch_size + lbl.shape[0]] = lbl

                        step += 1

                    ##
                    join_name = ".".join([exp_name, model_name, global_step, data_name])

                    '''
                    save_path=os.path.join(data_path, 'samples')

                    # for optimal cut calculation
                    #np.save(os.path.join(save_path, join_name + '.[0-0].labels.npy'), lbls)
                    #np.save(os.path.join(save_path, join_name + '.[0-0].probs.npy'), probs)

                    # sample figures for reporting
                    lbls, probs = _get_binarized(lbls, probs, cut_idx=1)
                    pred = np.argmax(probs, axis=1)
                    _imgs = imgs[np.logical_and(lbls == 1, pred == 1)]
                    _save_figures(_imgs, join_name+'.TP',
                                  save_path=save_path, show_fig=0)
                    _imgs = imgs[np.logical_and(lbls == 0, pred == 0)]
                    _save_figures(_imgs, join_name+'.TN',
                                  save_path=save_path, show_fig=0)
                    _imgs = imgs[np.logical_and(lbls == 1, pred == 0)]
                    _save_figures(_imgs, join_name+'.FN',
                                  save_path=save_path, show_fig=0)
                    _imgs = imgs[np.logical_and(lbls == 0, pred == 1)]
                    _save_figures(_imgs, join_name+'.FP',
                                  save_path=save_path, show_fig=0)
                    '''

                    roc_auc, fpr, tpr, cmat, sens, spec, accv = \
                        _get_result(lbls, probs,
                                    res_name=join_name,
                                    save_path=os.path.join(data_path, 'result')
                                    )

                    for i in range(1, num_classes):
                        print '[%d] auc:%.3f, acc:%.3f, sens:%.3f, spec:%.3f' % \
                              (i, roc_auc[i], accv[i], sens[i], spec[i])

                    _draw_ROC(roc_auc, fpr, tpr, num_classes,
                              title=join_name,
                              show_fig=False,
                              save_path=os.path.join(data_path, 'figures'))


                except Exception as e:  # pylint: disable=broad-except
                    print (e)



def _test_a_model(
        model_path,
        model_name,
        num_examples,
        num_classes,
        batch_size,
        images
):

    inference_name = 'inference_' + model_name
    if not inference_name in dir(model):
        raise ValueError('The model [%s] is not in model.py' %(model_name))

    logits = getattr(model, inference_name)(images=images, num_classes=num_classes, is_training=False)
    softmax = tf.nn.softmax(logits=logits, dim=-1)

    saver = tf.train.Saver()

    ckpt = tf.train.get_checkpoint_state(model_path)
    all_ckpt_paths = ckpt.all_model_checkpoint_paths
    num_ckpt = len(all_ckpt_paths)

    probs = np.zeros([num_ckpt, num_examples, num_classes])

    for ckpt_idx, ckpt_path in enumerate(all_ckpt_paths):
        print ('Restoring: ' + ckpt_path)
        global_step = ckpt_path.split('/')[-1].split('-')[-1]

        with tf.Session() as sess:
            saver.restore(sess, ckpt_path)

            try:
                num_iter = int(np.ceil(float(num_examples) / batch_size))
                step = 0

                while step < num_iter:  # and not coord.should_stop():
                    prob = sess.run([softmax])
                    probs[ckpt_idx, step * batch_size:step * batch_size + prob.shape[0]] = prob

                    step += 1

            except Exception as e:  # pylint: disable=broad-except
                print (e)

    probs = np.mean(probs, axis=0)

    return probs



def run_test(
        data_path,
        exp_name,
        model_name,
        val_name,
        trial_serial,
        test_name
):
    test_data_path = os.path.join(data_path, exp_name, test_name)
    model_path = os.path.join(data_path, exp_name, model_name)
    trial_serial_str = '%03d' %(trial_serial)
    val_ckpt_path = os.path.join(model_path, '%s_ckpt-%s' % (val_name, trial_serial_str))

    if not os.path.exists(val_ckpt_path):
        raise ValueError('No path:' + val_ckpt_path)

    # eval_dir = DATA_PATH + 'log/'
    batch_size = BATCH_SIZE
    num_classes = len(input.get_info(test_data_path, 'LABEL_LIST'))
    num_examples = _get_num_examples(test_data_path)

    print val_ckpt_path, num_classes, num_examples
    print test_data_path

    with tf.Graph().as_default() as g:

        images, labels = input.input(
            data_path=test_data_path,
            batch_size=batch_size,
            num_epochs=1,
            random_distortion=False,
            shuffle_batch=False
        )

        inference_name = 'inference_' + model_name
        if not inference_name in dir(model):
            raise ValueError('The model [%s] is not in model.py' %(model_name))

        logits = getattr(model, inference_name)(images=images, num_classes=num_classes, is_training=False)

        softmax = tf.nn.softmax(logits=logits, dim=-1)

        saver = tf.train.Saver()

        ckpt = tf.train.get_checkpoint_state(val_ckpt_path)
        if not ckpt:
            raise ValueError('No checkpoint found in ' + val_ckpt_path)
        all_ckpt_paths = ckpt.all_model_checkpoint_paths
        num_ckpt = len(all_ckpt_paths)

        probs = np.zeros([num_ckpt, num_examples, num_classes])
        imgs = np.zeros([num_examples, IMAGE_SIZE, IMAGE_SIZE, 1])
        lbls = np.zeros([num_examples], dtype=np.int32)

        for ckpt_idx, ckpt_path in enumerate(all_ckpt_paths):
            print ('Restoring: ' + ckpt_path)
            global_step = ckpt_path.split('/')[-1].split('-')[-1]

            with tf.Session() as sess:
                saver.restore(sess, ckpt_path)

                try:
                    num_iter = int(np.ceil(float(num_examples) / batch_size))
                    step = 0

                    #probs = np.empty([num_examples, num_classes])
                    #imgs = np.empty([num_examples, IMAGE_SIZE, IMAGE_SIZE, 1])
                    #lbls = np.zeros([num_examples], dtype=np.int32)
                    # nams = np.empty([num_iter*batch_size], dtype=object)

                    while step < num_iter:  # and not coord.should_stop():
                        prob, img, lbl = \
                            sess.run([softmax, images, labels])

                        probs[ckpt_idx, step * batch_size:step * batch_size + prob.shape[0]] = prob

                        if ckpt_idx == 0:
                            imgs[step * batch_size:step * batch_size + img.shape[0]] = img
                            lbls[step * batch_size:step * batch_size + lbl.shape[0]] = lbl

                        step += 1

                except Exception as e:  # pylint: disable=broad-except
                    print (e)

        probs = np.mean(probs, axis=0)

        ##
        join_name = ".".join([exp_name, model_name, val_name, trial_serial_str, 'all', test_name])

        '''
        save_path=os.path.join(data_path, 'samples')

        # for optimal cut calculation
        #np.save(os.path.join(save_path, join_name + '.[0-0].labels.npy'), lbls)
        #np.save(os.path.join(save_path, join_name + '.[0-0].probs.npy'), probs)

        # sample figures for reporting
        lbls, probs = _get_binarized(lbls, probs, cut_idx=1)
        pred = np.argmax(probs, axis=1)
        _imgs = imgs[np.logical_and(lbls == 1, pred == 1)]
        _save_figures(_imgs, join_name+'.TP',
                      save_path=save_path, show_fig=0)
        _imgs = imgs[np.logical_and(lbls == 0, pred == 0)]
        _save_figures(_imgs, join_name+'.TN',
                      save_path=save_path, show_fig=0)
        _imgs = imgs[np.logical_and(lbls == 1, pred == 0)]
        _save_figures(_imgs, join_name+'.FN',
                      save_path=save_path, show_fig=0)
        _imgs = imgs[np.logical_and(lbls == 0, pred == 1)]
        _save_figures(_imgs, join_name+'.FP',
                      save_path=save_path, show_fig=0)
        '''

        roc_auc, fpr, tpr, cmat, sens, spec, accv = \
            _get_result(lbls, probs,
                        res_name=join_name,
                        save_path=os.path.join(data_path, 'result')
                        )

        for i in range(1, num_classes):
            print '[%d] auc:%.3f, acc:%.3f, sens:%.3f, spec:%.3f' % \
                  (i, roc_auc[i], accv[i], sens[i], spec[i])

        _draw_ROC(roc_auc, fpr, tpr, num_classes,
                  title=join_name,
                  show_fig=False,
                  save_path=os.path.join(data_path, 'figures'))

def temp():
    getattr(model, 'test1')()

    #print not "_get_global_varss" in dir(model)


def main(_):

    os.environ["CUDA_VISIBLE_DEVICES"] = FLAGS.cuda_device

    if FLAGS.run_test==1:
        run_test(
            data_path=FLAGS.data_dir,
            exp_name=FLAGS.exp_name,
            model_name=FLAGS.model_name,
            val_name=FLAGS.val_name,
            trial_serial=FLAGS.trial_serial,
            test_name=FLAGS.test_name
        )
    else:
        run_validation(
            data_path=FLAGS.data_dir,
            exp_name=FLAGS.exp_name,
            model_name=FLAGS.model_name,
            val_name=FLAGS.val_name,
            trial_serial=FLAGS.trial_serial,
            interval=FLAGS.val_interval
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default=input.DATA_PATH,
                        help='Directory for storing input data')
    parser.add_argument('--log_dir', type=str, default='/data/SNUBH/sinusitis/',
                        help='Summaries log directory')
    parser.add_argument('--model_name', type=str, default=input.MODEL_NAME,
                        help='Model')
    parser.add_argument('--exp_name', type=str, default=input.EXP_NAME,
                        help='Experiment')
    parser.add_argument('--val_name', type=str, default=input.VAL_NAME,
                        help='Validation data folder name')
    parser.add_argument('--test_name', type=str, default=input.TEST_NAME,
                        help='Test data folder name')
    parser.add_argument('--trial_serial', type=int, default=input.TRIAL_SERIAL,
                        help='Trial serial')
    parser.add_argument('--cuda_device', type=str, default='3',
                        help='CUDA device ID list')
    parser.add_argument('--run_test', type=int, default=1,
                        help='Validation or Testing')
    parser.add_argument('--val_interval', type=int, default=10,
                        help='Validation interval for sleeping')

    FLAGS, unparsed = parser.parse_known_args()

    # print (FLAGS.max_steps)
    # print (unparsed)
    # print (sys.argv[0])

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)