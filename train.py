# from __future__ import absolute_import
# from __future__ import division
# from __future__ import print_function

import argparse
import sys

import numpy as np
import tensorflow as tf

import os
import json
import glob
import time
import re
import matplotlib.pyplot as plt

##
import model
import input

# os.environ["CUDA_VISIBLE_DEVICES"] = "0,1,2,3"

IMAGE_SIZE = input.IMAGE_SIZE
#NUM_CLASSES = input.NUM_CLASSES
BATCH_SIZE = input.BATCH_SIZE
PRETRAIN_MODEL_NAME = 'exp6/model/selected'

TOWER_NAME = 'tower'

INIT_LEARNING_RATE = 0.01
LR_DECAY_RATE = 0.94
LR_DECAY_STEPS = 1000
MAX_STEPS = 10000

def _show_figures(images, labels, names, probs,
                  image_width=IMAGE_SIZE,
                  image_height=IMAGE_SIZE,
                  num_rows=5,
                  num_cols=10,
                  figsize=(16, 9)):
    plt.figure(figsize=figsize)
    num_figs = images.shape[0]  # num_rows * num_cols
    num_chars = 5  # num of chars to show in names

    for j in range(num_figs):
        plt.subplot(num_rows, num_cols, j + 1)
        plt.imshow(images[j].reshape([image_height, image_width]), cmap='gray')
        plt.axis('off')
        name = 'dummy'  # names[j][:num_chars]
        pred = np.argmax(probs[j])
        label = labels[j]
        result = 'O' if label == pred else 'X'
        plt.title('%d(%.1f) / %d(%.1f)' % (label, probs[j][label], pred, probs[j][pred]),
                  fontsize=9)

    # mng = plt.get_current_fig_manager()
    # mng.window.showMaximized()
    plt.show()


def _remove_model_checkpoints(model_path):
    if tf.gfile.Exists(model_path):
        tf.gfile.DeleteRecursively(model_path)
    tf.gfile.MakeDirs(model_path)

# update 17-11-30
def run_training(
        data_path,
        exp_name,
        model_name,
        train_name,
        max_steps = MAX_STEPS,
        init_learning_rate = INIT_LEARNING_RATE,
        lr_decay_rate = LR_DECAY_RATE,
        lr_decay_steps = LR_DECAY_STEPS,
        resume_training = 0,
        remove_model_ckpt=True
):
    train_data_path = os.path.join(data_path, exp_name, train_name)
    model_path = os.path.join(data_path, exp_name, model_name)
    train_ckpt_path = os.path.join(model_path, '%s_ckpt'%(train_name))
    num_classes = len(input.get_info(train_data_path, 'LABEL_LIST'))

    print "Train data path:" + train_data_path

    #
    #if remove_model_ckpt:
    #    _remove_model_checkpoints(model_path)
    if not resume_training:
        print "New training"
        input.recreate_directory(train_ckpt_path)

    #resume_training = False #True

    with tf.Graph().as_default():
        global_step = tf.contrib.framework.get_or_create_global_step()

        with tf.device('/cpu:0'):
            images, labels = input.train_input(
                data_path=train_data_path,
                batch_size=BATCH_SIZE,
                num_epochs=None,  # infinite augmentation
                random_distortion=True,
                shuffle_batch=True
            )

        inference_name = 'inference_' + model_name
        if not inference_name in dir(model):
            raise ValueError('The model [%s] is not in model.py' %(model_name))

        logits = getattr(model, inference_name)(images=images, num_classes=num_classes)
        #logits = model.inference(images, num_classes)

        loss = model.loss(logits=logits, labels=labels)

        prob = tf.nn.softmax(logits, dim=-1)

        # train_op = model.train(loss, global_step)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            # learning rate & its decay
            learning_rate = tf.train.exponential_decay(
                init_learning_rate, global_step, lr_decay_steps, lr_decay_rate, staircase=True
            )

            train_op = model.train(loss, learning_rate, global_step)

        #
        saver = tf.train.Saver(max_to_keep=5)

        init_op = tf.group(tf.global_variables_initializer(),
                           tf.local_variables_initializer())

        with tf.Session() as sess:

            sess.run(init_op)
            # sess.run(train_init_ops)

            ckpt = tf.train.get_checkpoint_state(train_ckpt_path)
            if resume_training and ckpt:
                saver.restore(sess, ckpt.model_checkpoint_path)

            step = 0
            loss_sum = 0.0
            acc_sum = 0.0
            duration_sum = 0.0
            log_steps = 20
            #max_steps = 10000

            while True:
                try:
                    start_time = time.time()

                    _, loss_val, prob_val, img, lbl, lgs = \
                        sess.run([train_op, loss, prob, images, labels, logits])

                    duration = time.time() - start_time

                    prediction = np.equal(np.argmax(prob_val, axis=1), lbl)
                    acc_val = np.mean(prediction.astype('float32'))

                    loss_sum += loss_val
                    acc_sum += acc_val
                    duration_sum += duration

                    # Print an overview fairly often.
                    if step % log_steps == 0 and step > 1:
                        print('Step %d : loss = %.6f, acc = %.3f (avg %.3f sec)' %
                              (step, loss_sum / log_steps, acc_sum / log_steps, duration_sum / log_steps))
                        loss_sum = 0.0
                        acc_sum = 0.0
                        duration_sum = 0.0

                        # _show_figures(img, lbl, None, prob_val, num_rows=4, num_cols=12)

                        # Save checkpoints
                        # if step % 1000 == 0 and step > 1:
                        saver.save(sess,
                                   save_path=os.path.join(train_ckpt_path, 'model.ckpt'),
                                   global_step=global_step
                                   )

                    # Stopping criteria
                    # TODO: modify the criteria
                    if loss_val < 0.000001 or step > max_steps:  # or acc_val > 0.99:
                        saver.save(sess,
                                   save_path=os.path.join(train_ckpt_path, 'model.ckpt'),
                                   global_step=global_step
                                   )

                        print('Final loss: %.6f, accuracy: %.3f' % (loss_val, acc_val))
                        break

                        # _show_figures(img, lbl, None, prob_val)

                    step += 1
                except tf.errors.OutOfRangeError:
                    print('Done training for %d steps.' % (step))
                    break

##

def run_training_with_pretrained_network(
        data_path,
        exp_name,
        model_name,
        pretrain_model_name,
        max_steps = MAX_STEPS,
        init_learning_rate = INIT_LEARNING_RATE,
        lr_decay_rate = LR_DECAY_RATE,
        lr_decay_steps = LR_DECAY_STEPS
):
    train_data_path = os.path.join(data_path, exp_name, 'train/')
    model_path = os.path.join(data_path, exp_name, model_name)
    pretrain_model_path = os.path.join(data_path, pretrain_model_name)
    num_classes = len(input.get_info(train_data_path, 'LABEL_LIST'))


    with tf.Graph().as_default():
        global_step = tf.contrib.framework.get_or_create_global_step()

        with tf.device('/cpu:0'):
            images, labels = input.train_input(
                data_path=train_data_path,
                batch_size=BATCH_SIZE,
                num_epochs=None,  # infinite augmentation
                random_distortion=True,
                shuffle_batch=True
            )

        inference_name = 'inference_' + model_name
        if not inference_name in dir(model):
            raise ValueError('The model [%s] is not in model.py' %(model_name))

        logits = getattr(model, inference_name)(images=images, num_classes=num_classes)
        #logits = model.inference(images, num_classes)

        ############

        ckpt = tf.train.get_checkpoint_state(pretrain_model_path)
        all_ckpt_paths = ckpt.all_model_checkpoint_paths
        ckpt_path = all_ckpt_paths[-1]
        #global_step = ckpt_path.split('/')[-1].split('-')[-1]

        pretrain_vars = model.get_conv_vars()
        new_vars = model.get_fc_vars()

        ###
        print (ckpt_path)
        print len(pretrain_vars)
        print len(new_vars)
        for var in new_vars:
            print var.name

        #return

        loss = model.loss(logits=logits, labels=labels)

        prob = tf.nn.softmax(logits, dim=-1)

        # train_op = model.train(loss, global_step)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            # learning rate & its decay
            #lr_decay_steps = 1000
            #lr_decay_rate = 0.94
            #init_learning_rate = 0.01
            learning_rate = tf.train.exponential_decay(
                init_learning_rate, global_step, lr_decay_steps, lr_decay_rate, staircase=True
            )

            train_op = model.train(loss, learning_rate, global_step)

        saver0 = tf.train.Saver(pretrain_vars)

        saver = tf.train.Saver(max_to_keep=5)

        ###
        global_vars = tf.global_variables()
        new_vars = filter(lambda a: not a in pretrain_vars, global_vars)

        print len(new_vars)
        for var in new_vars:
            print var.name
        #return

        #init_op = tf.group(tf.global_variables_initializer(),
        #                   tf.local_variables_initializer())
        init_op = tf.group(tf.variables_initializer(var_list=new_vars),
                           tf.local_variables_initializer())

        with tf.Session() as sess:

            saver0.restore(sess, ckpt_path)

            sess.run(init_op)
            # sess.run(train_init_ops)

            step = 0
            loss_sum = 0.0
            acc_sum = 0.0
            duration_sum = 0.0
            log_steps = 20
            #max_steps = 10000

            while True:
                try:
                    start_time = time.time()

                    _, loss_val, prob_val, img, lbl, lgs = \
                        sess.run([train_op, loss, prob, images, labels, logits])

                    # print img.shape
                    # print lbl.shape
                    # print lbl
                    # print lgs

                    duration = time.time() - start_time

                    prediction = np.equal(np.argmax(prob_val, axis=1), lbl)
                    acc_val = np.mean(prediction.astype('float32'))

                    loss_sum += loss_val
                    acc_sum += acc_val
                    duration_sum += duration

                    # Print an overview fairly often.
                    if step % log_steps == 0 and step > 1:
                        print('Step %d: loss = %.6f, acc = %.3f (avg %.3f sec)' %
                              (step, loss_sum / log_steps, acc_sum / log_steps, duration_sum / log_steps))
                        loss_sum = 0.0
                        acc_sum = 0.0
                        duration_sum = 0.0

                        # _show_figures(img, lbl, None, prob_val, num_rows=4, num_cols=12)

                        # Save checkpoints
                        # if step % 1000 == 0 and step > 1:
                        saver.save(sess,
                                   save_path=os.path.join(model_path, 'model.ckpt'),
                                   global_step=global_step
                                   )

                    # Stopping criteria
                    # TODO: modify the criteria
                    if loss_val < 0.000001 or step > max_steps:  # or acc_val > 0.99:
                        saver.save(sess,
                                   save_path=os.path.join(model_path, 'model.ckpt'),
                                   global_step=global_step
                                   )

                        print('Final loss: %.6f, accuracy: %.3f' % (loss_val, acc_val))
                        break

                        # _show_figures(img, lbl, None, prob_val)

                    step += 1
                except tf.errors.OutOfRangeError:
                    print('Done training for %d steps.' % (step))
                    break


##############################3

def tower_loss(scope, images, labels):
    # Build inference Graph.

    logits = model.inference(images, is_training=True)

    # Build the portion of the Graph calculating the losses. Note that we will
    # assemble the total_loss using a custom function below.
    _loss, _acc, _prob = model.loss(logits, labels)

    # Assemble all of the losses for the current tower only.
    losses = tf.get_collection('losses', scope)

    # Calculate the total loss for the current tower.
    total_loss = tf.add_n(losses, name='total_loss')

    '''
    # Attach a scalar summary to all individual losses and the total loss; do the
    # same for the averaged version of the losses.
    for l in losses + [total_loss]:
        # Remove 'tower_[0-9]/' from the name in case this is a multi-GPU training
        # session. This helps the clarity of presentation on tensorboard.
        loss_name = re.sub('%s_[0-9]*/' % TOWER_NAME, '', l.op.name)
        tf.summary.scalar(loss_name, l)
    '''

    return total_loss


def average_gradients(tower_grads):
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(axis=0, values=grads)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)

    return average_grads


def run_training_multi_gpu(remove_model_ckpt=True):
    if remove_model_ckpt:
        _remove_model_checkpoints()

    batch_size = 50
    gpu_devices = os.environ["CUDA_VISIBLE_DEVICES"]
    gpu_nums = map(int, gpu_devices.split(','))
    num_gpus = len(gpu_nums)

    with tf.Graph().as_default(), tf.device('/cpu:0'):
        # Create a variable to count the number of train() calls. This equals the
        # number of batches processed * FLAGS.num_gpus.
        global_step = tf.get_variable(
            'global_step', [],
            initializer=tf.constant_initializer(0), trainable=False)

        # learning rate & its decay
        decay_steps = 1000
        decay_rate = 0.94
        init_learning_rate = 0.01
        learning_rate = tf.train.exponential_decay(
            init_learning_rate, global_step, decay_steps, decay_rate, staircase=True
        )

        # Create an optimizer that performs gradient descent.
        opt = tf.train.RMSPropOptimizer(
            learning_rate=learning_rate,
            decay=0.9,
            epsilon=0.1
        )

        #
        images, labels, names = data.input(batch_size=batch_size)
        batch_queue = tf.contrib.slim.prefetch_queue.prefetch_queue(
            [images, labels], capacity=2 * num_gpus)

        # Calculate the gradients for each model tower.
        tower_grads = []
        with tf.variable_scope(tf.get_variable_scope()):
            for i in gpu_nums:
                with tf.device('/gpu:%d' % i):
                    with tf.name_scope('%s_%d' % (TOWER_NAME, i)) as scope:
                        # Dequeues one batch for the GPU
                        image_batch, label_batch = batch_queue.dequeue()

                        # Calculate the loss for one tower of the model. This function
                        # constructs the entire model but shares the variables across
                        # all towers.
                        loss = tower_loss(scope, image_batch, label_batch)

                        # Reuse variables for the next tower.
                        tf.get_variable_scope().reuse_variables()

                        # update op for batch normalization
                        batchnorm_updates = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope)

                        # Retain the summaries from the final tower.
                        # summaries = tf.get_collection(tf.GraphKeys.SUMMARIES, scope)

                        # Calculate the gradients for the batch of data on this tower.
                        grads = opt.compute_gradients(loss)

                        # Keep track of the gradients across all towers.
                        tower_grads.append(grads)

        # We must calculate the mean of each gradient. Note that this is the
        # synchronization point across all towers.
        grads = average_gradients(tower_grads)

        # Add a summary to track the learning rate.
        # summaries.append(tf.summary.scalar('learning_rate', lr))

        # Add histograms for gradients.
        # for grad, var in grads:
        #    if grad is not None:
        #        summaries.append(tf.summary.histogram(var.op.name + '/gradients', grad))

        # Apply the gradients to adjust the shared variables.
        apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

        # Add histograms for trainable variables.
        # for var in tf.trainable_variables():
        #    summaries.append(tf.summary.histogram(var.op.name, var))

        # Track the moving averages of all trainable variables.
        # variable_averages = tf.train.ExponentialMovingAverage(
        #    cifar10.MOVING_AVERAGE_DECAY, global_step)
        # variables_averages_op = variable_averages.apply(tf.trainable_variables())

        update_ops = tf.group(*batchnorm_updates)
        # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

        # Group all updates to into a single train op.
        train_op = tf.group(apply_gradient_op, update_ops)  # , variables_averages_op)

        # Create a saver.
        saver = tf.train.Saver(tf.global_variables())

        # Build the summary operation from the last tower summaries.
        # summary_op = tf.summary.merge(summaries)

        # Build an initialization operation to run below.
        init = tf.global_variables_initializer()

        # Start running operations on the Graph. allow_soft_placement must be set to
        # True to build towers on GPU, as some of the ops do not have GPU
        # implementations.
        sess = tf.Session(config=tf.ConfigProto(
            gpu_options=tf.GPUOptions(allow_growth=True),
            allow_soft_placement=True,
            log_device_placement=False))
        sess.run(init)

        # Start the queue runners.
        tf.train.start_queue_runners(sess=sess)

        # summary_writer = tf.summary.FileWriter(FLAGS.train_dir, sess.graph)

        max_steps = 100000
        log_steps = 20
        loss_sum = 0.0
        acc_sum = 0.0
        duration_sum = 0.0

        for step in xrange(max_steps):
            start_time = time.time()
            _, loss_value = sess.run([train_op, loss])
            duration = time.time() - start_time

            assert not np.isnan(loss_value), 'Model diverged with loss = NaN'
            loss_sum += loss_value
            duration_sum += duration

            if step % log_steps == 0 and step > 1:
                num_examples_per_step = batch_size * num_gpus
                examples_per_sec = num_examples_per_step / duration
                sec_per_batch = duration / num_gpus

                print('Step %d: loss = %.6f, acc = %.3f (avg %.3f sec)' %
                      (step, loss_sum / log_steps, acc_sum / log_steps, duration_sum / log_steps))
                loss_sum = 0.0
                acc_sum = 0.0
                duration_sum = 0.0

            # if step % 100 == 0:
            #    summary_str = sess.run(summary_op)
            #    summary_writer.add_summary(summary_str, step)

            # Save the model checkpoint periodically.
            if step % 1000 == 0 and step > 1:  # loss_value < 0.001 or step > 100000:
                checkpoint_path = os.path.join(MODEL_PATH, 'model.ckpt')
                saver.save(sess, checkpoint_path, global_step=step)


def main(_):

    os.environ["CUDA_VISIBLE_DEVICES"] = FLAGS.cuda_device

    if FLAGS.pretrain_model == '0':
        print 'run training'
        run_training(
            data_path = FLAGS.data_dir,
            exp_name = FLAGS.exp_name,
            model_name = FLAGS.model_name,
            train_name=FLAGS.train_name,
            max_steps = FLAGS.max_steps,
            init_learning_rate = FLAGS.learning_rate,
            lr_decay_rate = FLAGS.decay_rate,
            lr_decay_steps = FLAGS.decay_steps,
            resume_training=FLAGS.resume_train,
            remove_model_ckpt=True
        )
    else:
        print 'run training with pretrained network'
        run_training_with_pretrained_network(
            data_path = FLAGS.data_dir,
            exp_name = FLAGS.exp_name,
            model_name = FLAGS.model_name,
            pretrain_model_name=FLAGS.pretrain_model,
            max_steps = FLAGS.max_steps,
            init_learning_rate = FLAGS.learning_rate,
            lr_decay_rate = FLAGS.decay_rate,
            lr_decay_steps = FLAGS.decay_steps
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max_steps', type=int, default=MAX_STEPS,
                        help='Number of steps to run trainer.')
    parser.add_argument('--learning_rate', type=float, default=INIT_LEARNING_RATE,
                        help='Initial learning rate')
    parser.add_argument('--decay_rate', type=float, default=LR_DECAY_RATE,
                        help='Learning rate decay')
    parser.add_argument('--decay_steps', type=float, default=LR_DECAY_STEPS,
                        help='Learning rate decay steps')

    parser.add_argument('--dropout', type=float, default=0.9,
                        help='Keep probability for training dropout.')

    parser.add_argument('--data_dir', type=str, default=input.DATA_PATH,
                        help='Directory for storing input data')
    parser.add_argument('--log_dir', type=str, default='/data/SNUBH/sinusitis/',
                        help='Summaries log directory')
    parser.add_argument('--train_name', type=str, default=input.TRAIN_NAME,
                        help='Train data folder name')
    parser.add_argument('--model_name', type=str, default=input.MODEL_NAME,
                        help='Model')
    parser.add_argument('--exp_name', type=str, default=input.EXP_NAME,
                        help='Experiment')
    parser.add_argument('--cuda_device', type=str, default='2',
                        help='CUDA device ID list')
    parser.add_argument('--resume_train', type=int, default=0,
                        help='Resume training')
    parser.add_argument('--pretrain_model', type=str, default='0',
                        help='Path to pretrained model checkpoint')
    FLAGS, unparsed = parser.parse_known_args()

    # print (FLAGS.max_steps)
    # print (unparsed)
    # print (sys.argv[0])

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
