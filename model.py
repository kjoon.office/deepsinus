import tensorflow as tf
from tensorflow.contrib.layers.python.layers.layers import batch_norm
from tensorflow.contrib.layers.python.layers import xavier_initializer as xavier

def _variable_on_cpu(name, shape, initializer):
    """Helper to create a Variable stored on CPU memory. """
    with tf.device('/cpu:0'):
        dtype = tf.float32
        var = tf.get_variable(name, shape, initializer=initializer, dtype=dtype)
    return var

def _variable_with_weight_decay(name, shape, initializer, wd):

    dtype = tf.float32
    var = _variable_on_cpu(name, shape, initializer=initializer)
    if wd is not None:
        weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
        tf.add_to_collection('losses', weight_decay)
    return var



def _conv2d(input, kernel, layer_name, is_training):
    with tf.variable_scope(layer_name):     # ? name_scope raises error in multi-gpu
        Conv = tf.nn.conv2d(input, kernel, strides=(1, 1, 1, 1), padding='SAME')
        BN3D = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        out = tf.nn.relu(BN3D)
    return out


def _convfc3d(input, kernel, layer_name, is_training):
    with tf.variable_scope(layer_name):
        Conv = tf.nn.conv2d(input, kernel, strides=(1, 1, 1, 1), padding='VALID')
        BN3D = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        out = tf.nn.relu(BN3D)
    return out


def _pooling3d(input, layer_name):
    with tf.variable_scope(layer_name):
        out = tf.nn.max_pool(input, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1), padding='SAME')
    return out


def _resblock1(input, kernel1, kernel2, kernel3, layer_name, is_training):
    with tf.variable_scope(layer_name):
        Conv = tf.nn.conv2d(input, kernel1, strides=(1, 1, 1, 1), padding='SAME')
        BN = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        Conv = tf.nn.conv2d(tf.nn.relu(BN), kernel2, strides=(1, 1, 1, 1), padding='SAME')
        BN = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        input_dim_increase = tf.nn.conv2d(input, kernel3, strides=(1, 1, 1, 1), padding='SAME')
        BN_input = batch_norm(input_dim_increase, data_format='NHWC', center=True, scale=True, is_training=is_training)
        out = tf.nn.relu(BN + BN_input)
    return out


def _resblock2(input, kernel1, kernel2, layer_name, is_training):
    with tf.variable_scope(layer_name):
        Conv = tf.nn.conv2d(input, kernel1, strides=(1, 1, 1, 1), padding='SAME')
        BN = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        Conv = tf.nn.conv2d(tf.nn.relu(BN), kernel2, strides=(1, 1, 1, 1), padding='SAME')
        BN = batch_norm(Conv, data_format='NHWC', center=True, scale=True, is_training=is_training)
        out = tf.nn.relu(BN + input)
    return out

#### change order

def _resblock1_2(l, kernel1, kernel2, kernel3, layer_name, is_training):
    with tf.variable_scope(layer_name):
        c = batch_norm(l, data_format='NHWC', center=True, scale=True, is_training=is_training,scope=layer_name+'1')
        c1 = tf.nn.relu(c)
        c = tf.nn.conv2d(c1,kernel1,strides=(1,1,1,1),padding='SAME')
        c = batch_norm(c, data_format='NHWC', center=True, scale=True, is_training=is_training,scope=layer_name+'2')
        c = tf.nn.relu(c)
        c = tf.nn.conv2d(c,kernel2,strides=(1,1,1,1),padding='SAME')
        l = tf.nn.conv2d(c1, kernel3, strides=(1, 1, 1, 1), padding='SAME')
    return c+l

def _resblock2_2(l, kernel1, kernel2, layer_name, is_training):
    with tf.variable_scope(layer_name):
        c = batch_norm(l, data_format='NHWC', center=True, scale=True, is_training=is_training,scope=layer_name+'1')
        c = tf.nn.relu(c)
        c = tf.nn.conv2d(c,kernel1,strides=(1,1,1,1),padding='SAME')
        c = batch_norm(c, data_format='NHWC', center=True, scale=True, is_training=is_training,scope=layer_name+'2')
        c = tf.nn.relu(c)
        c = tf.nn.conv2d(c, kernel2, strides=(1, 1, 1, 1), padding='SAME')
    return c+l

def test1():
    print 'model.test1'

def test2():
    print 'model.test2'

def _get_global_vars(scope_list):
    _vars = []
    for scope in scope_list:
        _vars += tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope)

    return _vars

def get_conv_vars():
    scope_list = ['w1', 'w2_1', 'w2_2', 'w2_3', 'w2_4',
                  'w3_1x1', 'w3_1', 'w3_2', 'w3_3', 'w3_4',
                  'w4_1x1', 'w4_1', 'w4_2', 'w4_3', 'w4_4',
                  'w5_1x1', 'w5_1', 'w5_2', 'w5_3', 'w5_4',
                  'conv1', 'conv2_1', 'conv2_2', 'conv3_1', 'conv3_2',
                  'conv4_1', 'conv4_2', 'conv5_1', 'conv5_2'
                  ]
    _vars = _get_global_vars(scope_list)
    return _vars

def get_fc_vars():
    scope_list = ['fc1', 'fc2', 'fc3']
    _vars = _get_global_vars(scope_list)
    return _vars

def inference_model(images, num_classes, is_training = True):
    w1 = tf.get_variable(name='w1', shape=[3, 3, 1, 64], initializer=xavier())
    w2_1 = tf.get_variable(name='w2_1', shape=[3, 3, 64, 64], initializer=xavier())
    w2_2 = tf.get_variable(name='w2_2', shape=[3, 3, 64, 64], initializer=xavier())
    w2_3 = tf.get_variable(name='w2_3', shape=[3, 3, 64, 64], initializer=xavier())
    w2_4 = tf.get_variable(name='w2_4', shape=[3, 3, 64, 64], initializer=xavier())
    w3_1x1 = tf.get_variable(name='w3_1x1', shape=[1, 1, 64, 128], initializer=xavier())
    w3_1 = tf.get_variable(name='w3_1', shape=[3, 3, 64, 128], initializer=xavier())
    w3_2 = tf.get_variable(name='w3_2', shape=[3, 3, 128, 128], initializer=xavier())
    w3_3 = tf.get_variable(name='w3_3', shape=[3, 3, 128, 128], initializer=xavier())
    w3_4 = tf.get_variable(name='w3_4', shape=[3, 3, 128, 128], initializer=xavier())
    w4_1x1 = tf.get_variable(name='w4_1x1', shape=[1, 1, 128, 256], initializer=xavier())
    w4_1 = tf.get_variable(name='w4_1', shape=[3, 3, 128, 256], initializer=xavier())
    w4_2 = tf.get_variable(name='w4_2', shape=[3, 3, 256, 256], initializer=xavier())
    w4_3 = tf.get_variable(name='w4_3', shape=[3, 3, 256, 256], initializer=xavier())
    w4_4 = tf.get_variable(name='w4_4', shape=[3, 3, 256, 256], initializer=xavier())
    w5_1x1 = tf.get_variable(name='w5_1x1', shape=[1, 1, 256, 512], initializer=xavier())
    w5_1 = tf.get_variable(name='w5_1', shape=[3, 3, 256, 512], initializer=xavier())
    w5_2 = tf.get_variable(name='w5_2', shape=[3, 3, 512, 512], initializer=xavier())
    w5_3 = tf.get_variable(name='w5_3', shape=[3, 3, 512, 512], initializer=xavier())
    w5_4 = tf.get_variable(name='w5_4', shape=[3, 3, 512, 512], initializer=xavier())
    fc1 = tf.get_variable(name='fc1', shape=[7, 7, 512, 4096], initializer=xavier())
    #fc1 = _variable_with_weight_decay(
    #    name='fc1', shape=[7, 7, 512, 4096], initializer=xavier(), wd=0.004)
    fc2 = tf.get_variable(name='fc2', shape=[1, 1, 4096, 4096], initializer=xavier())
    #fc2 = _variable_with_weight_decay(
    #    name='fc2', shape=[1, 1, 4096, 4096], initializer=xavier(), wd=0.004)
    fc3 = tf.get_variable(name='fc3', shape=[1, 1, 4096, num_classes], initializer=xavier())
    #fc3b = tf.Variable(tf.zeros([2]), name='fc3b')

    ##
    conv1 = _conv2d(images, w1, 'conv1', is_training=is_training)
    pool1 = _pooling3d(conv1, 'pool1')

    conv2 = _resblock2(pool1, w2_1, w2_2, 'conv2_1', is_training=is_training)
    conv2 = _resblock2(conv2, w2_3, w2_4, 'conv2_2', is_training=is_training)
    pool2 = _pooling3d(conv2, 'pool2')

    conv3 = _resblock1(pool2, w3_1, w3_2, w3_1x1, 'conv3_1', is_training=is_training)
    conv3 = _resblock2(conv3, w3_3, w3_4, 'conv3_2', is_training=is_training)
    pool3 = _pooling3d(conv3, 'pool3')

    conv4 = _resblock1(pool3, w4_1, w4_2, w4_1x1, 'conv4_1', is_training=is_training)
    conv4 = _resblock2(conv4, w4_3, w4_4, 'conv4_2', is_training=is_training)
    pool4 = _pooling3d(conv4, 'pool4')

    conv5 = _resblock1(pool4, w5_1, w5_2, w5_1x1, 'conv5_1', is_training=is_training)
    conv5 = _resblock2(conv5, w5_3, w5_4, 'conv5_2', is_training=is_training)
    pool5 = _pooling3d(conv5, 'pool5')

    fc1_out = _convfc3d(pool5, fc1, 'fc1', is_training=is_training)
    fc2_out = _convfc3d(fc1_out, fc2, 'fc2', is_training=is_training)

    with tf.name_scope('fc3'):
        logits = tf.squeeze(tf.nn.conv2d(fc2_out, fc3, strides=(1, 1, 1, 1), padding='VALID')) #+ fc3b

    return logits


def inference_model2(images, num_classes, is_training = True):
    w1 = tf.get_variable(name='w1', shape=[3, 3, 1, 64], initializer=xavier())
    w2_1 = tf.get_variable(name='w2_1', shape=[3, 3, 64, 64], initializer=xavier())
    w2_2 = tf.get_variable(name='w2_2', shape=[3, 3, 64, 64], initializer=xavier())
    w2_3 = tf.get_variable(name='w2_3', shape=[3, 3, 64, 64], initializer=xavier())
    w2_4 = tf.get_variable(name='w2_4', shape=[3, 3, 64, 64], initializer=xavier())
    w3_1x1 = tf.get_variable(name='w3_1x1', shape=[1, 1, 64, 128], initializer=xavier())
    w3_1 = tf.get_variable(name='w3_1', shape=[3, 3, 64, 128], initializer=xavier())
    w3_2 = tf.get_variable(name='w3_2', shape=[3, 3, 128, 128], initializer=xavier())
    w3_3 = tf.get_variable(name='w3_3', shape=[3, 3, 128, 128], initializer=xavier())
    w3_4 = tf.get_variable(name='w3_4', shape=[3, 3, 128, 128], initializer=xavier())
    w4_1x1 = tf.get_variable(name='w4_1x1', shape=[1, 1, 128, 256], initializer=xavier())
    w4_1 = tf.get_variable(name='w4_1', shape=[3, 3, 128, 256], initializer=xavier())
    w4_2 = tf.get_variable(name='w4_2', shape=[3, 3, 256, 256], initializer=xavier())
    w4_3 = tf.get_variable(name='w4_3', shape=[3, 3, 256, 256], initializer=xavier())
    w4_4 = tf.get_variable(name='w4_4', shape=[3, 3, 256, 256], initializer=xavier())

    w5_1x1 = tf.get_variable(name='w5_1x1', shape=[1, 1, 256, 400], initializer=xavier())
    w5_1 = tf.get_variable(name='w5_1', shape=[3, 3, 256, 400], initializer=xavier())
    w5_2 = tf.get_variable(name='w5_2', shape=[3, 3, 400, 400], initializer=xavier())
    w5_3 = tf.get_variable(name='w5_3', shape=[3, 3, 400, 400], initializer=xavier())
    w5_4 = tf.get_variable(name='w5_4', shape=[3, 3, 400, 400], initializer=xavier())
    w6_1x1 = tf.get_variable(name='w6_1x1', shape=[1, 1, 400, 512], initializer=xavier())
    w6_1 = tf.get_variable(name='w6_1', shape=[3, 3, 400, 512], initializer=xavier())
    w6_2 = tf.get_variable(name='w6_2', shape=[3, 3, 512, 512], initializer=xavier())
    w6_3 = tf.get_variable(name='w6_3', shape=[3, 3, 512, 512], initializer=xavier())
    w6_4 = tf.get_variable(name='w6_4', shape=[3, 3, 512, 512], initializer=xavier())


    ##
    fc3 = tf.get_variable(name='fc3', shape=[1, 1, 512, num_classes], initializer=xavier())

    ##
    conv1 = _conv2d(images, w1, 'conv1', is_training=is_training)
    pool1 = _pooling3d(conv1, 'pool1')

    conv2 = _resblock2(pool1, w2_1, w2_2, 'conv2_1', is_training=is_training)
    conv2 = _resblock2(conv2, w2_3, w2_4, 'conv2_2', is_training=is_training)
    pool2 = _pooling3d(conv2, 'pool2')

    conv3 = _resblock1(pool2, w3_1, w3_2, w3_1x1, 'conv3_1', is_training=is_training)
    conv3 = _resblock2(conv3, w3_3, w3_4, 'conv3_2', is_training=is_training)
    pool3 = _pooling3d(conv3, 'pool3')

    conv4 = _resblock1(pool3, w4_1, w4_2, w4_1x1, 'conv4_1', is_training=is_training)
    conv4 = _resblock2(conv4, w4_3, w4_4, 'conv4_2', is_training=is_training)
    pool4 = _pooling3d(conv4, 'pool4')

    conv5 = _resblock1(pool4, w5_1, w5_2, w5_1x1, 'conv5_1', is_training=is_training)
    conv5 = _resblock2(conv5, w5_3, w5_4, 'conv5_2', is_training=is_training)
    pool5 = _pooling3d(conv5, 'pool5')

    conv = _resblock1(pool5, w6_1, w6_2, w6_1x1, 'conv6_1', is_training=is_training)
    conv = _resblock2(conv,  w6_3, w6_4, 'conv6_2', is_training=is_training)
    conv = batch_norm(conv, data_format='NHWC', center=True, scale=True, is_training=is_training, scope='bn_last')
    conv = tf.nn.relu(conv)
    pool = tf.nn.avg_pool(conv, ksize=(1, 7, 7, 1), strides=(1, 1, 1, 1), padding='VALID')

    with tf.name_scope('fc3'):
        logits = tf.squeeze(tf.nn.conv2d(pool, fc3, strides=(1, 1, 1, 1), padding='VALID')) #+ fc3b

    return logits



def loss(logits, labels):
    labels = tf.cast(labels, tf.int64)
    use_sparse_label = True
    if use_sparse_label:
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=labels, logits=logits, name='cross_entropy_per_example'
        )
    else:
        onehot_labels = tf.one_hot(labels, 2, 1, 0)
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
            labels=onehot_labels, logits=logits, name='cross_entropy_per_example'
        )

    loss = tf.reduce_mean(cross_entropy)
    tf.add_to_collection('losses', loss)
    total_loss = tf.add_n(tf.get_collection('losses'), name='total_loss')

    #prob = tf.nn.softmax(logits, dim=-1)
    #prediction = tf.equal(tf.argmax(prob,1), labels)
    #acc = tf.reduce_mean(tf.cast(prediction, tf.float32))

    return total_loss


def prediction(logits, labels):
    labels = tf.cast(labels, tf.int64)
    prob = tf.nn.softmax(logits, dim=-1)
    return tf.equal(tf.argmax(prob,1), labels)


def train(loss, learning_rate, global_step):

    # optimizer
    train_op = tf.train.RMSPropOptimizer(
        learning_rate=learning_rate,
        decay=0.9,
        epsilon=0.1
    ).minimize(loss, global_step=global_step)

    return train_op