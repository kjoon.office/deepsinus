# Input to the model
# 09/18/17, kjoon@snubh.org

import tensorflow as tf
import numpy as np
import dicom

import time
import os
import glob
import json

import matplotlib.pyplot as plt
from skimage.transform import resize
from scipy.misc import imresize
from os.path import basename, splitext

#
import data

#
# Global constants
#

IMAGE_SIZE = data.IMAGE_SIZE
BATCH_SIZE = 50

DATA_PATH = '/data/SNUBH/sinusitis/'
EXP_NAME = 'exp18'
MODEL_NAME = 'model'
VAL_NAME = 'val'
TRIAL_SERIAL = 1
TEST_NAME = 'test'
TRAIN_NAME = 'train'


'''
def _crop_resize_flip(img, coord, radius, spacing, width, height, flip=False, reverse=0):

    crop_width = int(radius / spacing['x'])
    crop_height = int(radius / spacing['y'])
    assert crop_width > 1 and crop_height > 1

    if reverse==1:
        white_img = np.full_like(img, np.max(img), img.dtype)
        img = np.subtract(white_img, img)

    img = img[max(0, coord['y']-crop_height):min(img.shape[0], coord['y']+crop_height),
              max(0, coord['x']-crop_width) :min(img.shape[1], coord['x']+crop_width)]
    img = resize(img, (height, width), preserve_range=True)     # returns float64
    img = img.astype('float32')
    if flip: img = np.fliplr(img)

    #print(type(img))
    #print(img.dtype)
    #print(img.shape)
    #print((np.max(img), np.min(img)))

    #plt.imshow(img, cmap = 'gray')
    #plt.show()

    return img
'''

def recreate_directory(path):
    if tf.gfile.Exists(path):
        tf.gfile.DeleteRecursively(path)
    tf.gfile.MakeDirs(path)

def _random_distortion(image, label):
    width = IMAGE_SIZE
    height = IMAGE_SIZE
    k = tf.constant([_k for _k in range(-48, 49, 4)])
    k = tf.random_shuffle(k)

    #image = tf.reshape(image, [IMAGE_SIZE_BEFORE_CROP, IMAGE_SIZE_BEFORE_CROP, 1])
    #image.set_shape([250, 250, 1])
    #image = tf.random_crop(image, [height-k[0], width-k[1], 1])
    image = tf.image.resize_image_with_crop_or_pad(
        image, height+k[1], width+k[0])
    image = tf.image.resize_images(image, [height, width])
    #image = tf.image.random_flip_left_right(image)
    #image = tf.image.rot90(image, k=k[0])
    image = tf.image.random_contrast(image, lower=0.2, upper=1.8)

    image = tf.image.per_image_standardization(image)
    image.set_shape([height, width, 1])

    return image, label


def _image_standardization(image, label):
    width = IMAGE_SIZE
    height = IMAGE_SIZE

    #image = tf.reshape(image, [IMAGE_SIZE_BEFORE_CROP, IMAGE_SIZE_BEFORE_CROP, 1])
    image = tf.image.resize_image_with_crop_or_pad(image, height, width)

    image = tf.image.per_image_standardization(image)
    image.set_shape([height, width, 1])

    return image, label

def _read_and_decode2(serialized_example):
    width = IMAGE_SIZE
    height = IMAGE_SIZE

    #print filename_queue

    #reader = tf.TFRecordReader()
    #_, serialized_example = reader.read(filename_queue)

    features = tf.parse_single_example(
        serialized=serialized_example,
        features={
            #'name': tf.FixedLenFeature([], tf.string),
            'image': tf.FixedLenFeature([], tf.string),
            'label': tf.FixedLenFeature([], tf.int64)
        }
    )
    ##
    image = tf.decode_raw(features['image'], tf.float32)
    image = tf.reshape(image, [height, width, 1])

    label = tf.cast(features['label'], tf.int32)
    #name = tf.cast(features['name'], tf.string)

    return image, label

def _get_dataset(filenames, batch_size, random_distortion, shuffle_batch, num_epochs):

    num_threads = 8         # cpu threads to fill input queue
    map_output_buffer_size = batch_size * 100
    shuffle_buffer_size = batch_size * 100

    dataset = tf.contrib.data.TFRecordDataset(filenames)

    ## map functions
    dataset = dataset.map(
        _read_and_decode2,
        num_threads=num_threads
        #output_buffer_size=map_output_buffer_size
    )

    if random_distortion:
        dataset = dataset.map(
            _random_distortion,
            num_threads=num_threads
            #output_buffer_size=map_output_buffer_size
        )
    else:
        dataset = dataset.map(
            _image_standardization,
            num_threads=num_threads
            #output_buffer_size=map_output_buffer_size
        )

    ##
    if shuffle_batch:
        dataset = dataset.shuffle(buffer_size=shuffle_buffer_size)
        # Equivalent to min_after_dequeue.
    dataset = dataset.batch(batch_size)
    dataset = dataset.repeat(num_epochs)

    return dataset

def get_info(data_path, key):
    with open(os.path.join(data_path, '.info'), 'r') as f:
        info = json.load(f)
        return info[key]

# balanced input
def train_input(
    data_path,
    batch_size,
    num_epochs = None,
    random_distortion = True,
    shuffle_batch = True
):

    label_list = get_info(data_path, 'LABEL_LIST')
    num_classes = len(label_list)
    batch_size_per_class = int(batch_size / num_classes)

    print label_list, data_path, batch_size_per_class

    #init_ops = []
    image_batches = []
    label_batches = []
    for c in label_list:
        filenames = glob.glob(os.path.join(data_path, '*_label%s_*.tfrecord'%(c)))
        #print filenames
        if len(filenames) == 0: continue

        ds = _get_dataset(filenames, batch_size_per_class,
                          random_distortion, shuffle_batch, num_epochs)
        iter = ds.make_one_shot_iterator()
        #iter = tf.contrib.data.Iterator.from_structure(ds.output_types, ds.output_shapes)
        imgs, lbls = iter.get_next()
        #init_op = iter.make_initializer(ds)

        #init_ops += [init_op]
        image_batches += [imgs]
        label_batches += [lbls]

    #assert len(init_ops) > num_classes-1
    assert len(image_batches) > num_classes - 1

    images = tf.concat(image_batches, axis=0)
    labels = tf.concat(label_batches, axis=0)

    #return init_ops, images, labels
    return images, labels

def input(
        data_path,
        batch_size,
        num_epochs = 1,
        random_distortion = False,
        shuffle_batch = True
):
    #
    filenames = glob.glob(os.path.join(data_path, '*.tfrecord'))
    if len(filenames) == 0:
        raise ValueError('No tfrecord file in ' + data_path)

    #batch_image, batch_label = \
    #    _get_next(filenames, batch_size, random_distortion, shuffle_batch, num_epochs)
    ds = _get_dataset(filenames, batch_size, random_distortion, shuffle_batch, num_epochs)

    #iter = tf.contrib.data.Iterator.from_structure(ds.output_types, ds.output_shapes)
    iter = ds.make_one_shot_iterator()

    images, labels = iter.get_next()
    #init_op = iter.make_initializer(ds)

    #return init_op, images, labels
    return images, labels

def _mytest_input():

    os.environ["CUDA_VISIBLE_DEVICES"] = "3"

    train_images, train_labels = train_input(
        data_path=DATA_PATH + 'exp17/train/',
        batch_size=50,
        num_epochs = None,
        random_distortion=False, #True,
        shuffle_batch=False
    )

    exp_name = 'exp14'
    data_name = 'test'
    info_str = 'std1'

    val_images, val_labels = input(
        data_path=os.path.join(DATA_PATH, exp_name, data_name),
        batch_size=50,
        num_epochs = 1,
        random_distortion=False,
        shuffle_batch=False
    )

    #is_training = tf.placeholder(tf.bool)
    #images = tf.cond(is_training, lambda: train_images, lambda: val_images)
    #labels = tf.cond(is_training, lambda: train_labels, lambda: val_labels)

    with tf.Session() as sess:
        step = 0
        show_examples = True

        # train data
        #sess.run(train_init_ops)
        while 0: #step < 60:
            try:
                img, lbl = sess.run([train_images, train_labels])
                print 'train', step, img.shape, lbl
                if show_examples:
                    height = img.shape[1]
                    width = img.shape[2]
                    plt.figure(figsize=(16,9))
                    for j in range(min(img.shape[0],48)):
                        plt.subplot(4, 12, j + 1)
                        plt.imshow(img[j].reshape([height, width]), cmap='gray')
                        plt.title('%d'%(lbl[j]), fontsize=9)
                        plt.axis('off')
                    plt.show()
            except tf.errors.OutOfRangeError:
                print 'training ends'
                break
            step += 1

        # val data
        #sess.run(val_init_op)

        while 1: #step < 1:
            try:
                img, lbl = sess.run([val_images, val_labels])
                print 'test', step, img.shape
                if show_examples:
                    height = img.shape[1]
                    width = img.shape[2]
                    plt.figure(figsize=(16,9))
                    for j in range(min(img.shape[0],48)):
                        plt.subplot(4, 12, j + 1)
                        mm = np.mean(img[j])
                        plt.imshow(img[j].reshape([height, width]), cmap='gray', vmin=-2, vmax=2)
                        plt.title('%d'%(lbl[j]), fontsize=9)
                        #plt.title('%.2f' % (mm), fontsize=9)
                        plt.axis('off')
                    #plt.show()
                    plt.savefig(
                        os.path.join(
                            'input_figures',
                            exp_name+'_'+data_name+'_'+str(step)+'_'+info_str+'.png'
                        )
                    )
                    plt.close()
            except tf.errors.OutOfRangeError:
                print 'test ends'
                break

            step += 1

##

def _mytest_multi_dataset():
    inc_dataset = tf.contrib.data.Dataset.range(10)
    dec_dataset = tf.contrib.data.Dataset.range(0, -12, -1)

    #inc_dataset.map(
    #    lambda x: x + tf.random_uniform([], 0, 2, tf.int64))


    #dec_dataset = dec_dataset.map(
    #    lambda x: x + tf.random_uniform([], -2, 0, tf.int64))


    #dataset = tf.contrib.data.Dataset.zip((inc_dataset, dec_dataset))
    #dataset.repeat()
    #dec_dataset = dec_dataset.shuffle(buffer_size=20)
    dec_dataset = dec_dataset.batch(5)
    #dec_dataset = dec_dataset.repeat()
    iterator1 = dec_dataset.make_one_shot_iterator()
    next_element1 = iterator1.get_next()

    #inc_dataset = inc_dataset.shuffle(buffer_size=20)
    inc_dataset = inc_dataset.batch(5)
    inc_dataset = inc_dataset.repeat()
    iterator2 = inc_dataset.make_one_shot_iterator()
    next_element2 = iterator2.get_next()

    next_element = tf.concat([next_element1, next_element2], axis=0)

    errcnt = 0
    with tf.Session() as sess:
        while True:
            try:
                batch = sess.run(next_element)
                print(batch.shape[0], batch)
            except tf.errors.OutOfRangeError:
                print('out of range:', errcnt)
                print(batch.shape[0], batch)
                if errcnt > 3:
                    break
                errcnt += 1

#
# Dynamic input generation
#

def _read_crop_resize2(filename, label):
    width = IMAGE_SIZE
    height = IMAGE_SIZE

    try:
        dcm = dicom.read_file(filename)
    except:
        raise ValueError('No file: '+filename)

    img = dcm.pixel_array
    img = img[0:min(img.shape), 0:min(img.shape)]
    img = resize(img, (width, height), preserve_range=True)     # returns float64
    img = img.astype('float32')
    img = np.reshape(img, [height, width, 1])

    return img, label

def _mytest_dynamic_input(data_type='val', data_path=DATA_PATH, batch_size=20):

    try:
        data = np.load(data_path + data_type + '_list.npy')
    except:
        raise ValueError('No file: '+ data_path + data_type + '_list.npy')

    data = data.item()
    files = data['files']
    labels = data['labels']


    ds = tf.contrib.data.Dataset

    dataset = ds.from_tensor_slices((files, labels))

    # NOTE: https://github.com/tensorflow/tensorflow/issues/11786
    # tells reason for "tuple"(tf.py_func..

    dataset = dataset.map(
        lambda filename, label: tuple(tf.py_func(
            _read_crop_resize2, [filename, label], [tf.float32, label.dtype]
        )),
        num_threads=8,
        output_buffer_size=batch_size*10
    )
    dataset = dataset.map(_random_distortion)

    #dataset = dataset.repeat(10)
    dataset = dataset.batch(batch_size)

    iter = dataset.make_one_shot_iterator()
    next_batch = iter.get_next()

    dummy_op = tf.no_op()


    with tf.Session() as sess:
        step = 0
        show_examples = True
        while True or step > 10:
            try:
                _, (img, lbl) = sess.run([dummy_op, next_batch])
                height = img.shape[1]
                width = img.shape[2]
                #print (step, val[1])
                if show_examples:
                    for j in range(batch_size):
                        plt.subplot(4, 5, j + 1)
                        plt.imshow(img[j].reshape([height, width]), cmap='gray')
                        plt.title(('neg' if lbl[j] == 0 else 'pos'), fontsize=9)
                        plt.axis('off')
                    plt.show()
            except tf.errors.OutOfRangeError:
                break

            step += 1



if __name__ == '__main__':

    _mytest_input()

